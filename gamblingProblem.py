#author: Georgios Pierris
#website: www.pierris.gr

# run for 1000 episodes, 10 attempts, ticke price 40 dollars 
# python gamblingProblem.py 1000 10 40

from random import *
import sys
import pylab
import numpy as np



def main():


    if(len(sys.argv)==4):
        numOfEpisodes = int(sys.argv[1]);
        numOfAttempts = int(sys.argv[2])
        ticketPrice = float(sys.argv[3])

    else:
        print '\nError in number of arguments. Usage: \n\npython gamblingProblem.py X Y Z \n\nX: Number of episodes\nY: Number of attempts per episode\nZ: Price of ticket'
        print '\n\n'
        sys.exit()

    cashEachEpisode = []
    cashEachEpisodeRandom = []
    probabilityOfWinningEachEpisode = []

    for episode in range(numOfEpisodes):

        cash = 0
        wins = 0
        losses = 0
        earnings = []

        cashRandom = 0
        winsRandom = 0
        lossesRandom = 0

        trackOfCash = []
        trackOfCashRandom = []
        trackOfProbability = []


        for attempt in range(numOfAttempts):
            #print attempt,', ',

            x = randrange(0,101)
            y = randrange(0,101)
            cash = cash - ticketPrice #I pay the ticket to enter the attempt
            cashRandom = cashRandom - ticketPrice

            #Be a rational player

            if(x >= 50): #Then I believe that y is less

                if( y < x ): #If my guess was correct
                    cash = cash + y #Then I win y dollars
                    wins = wins + 1
                    earnings.append(y)

                elif( y == x):
                    cash = cash + y/2.0
                    earnings.append(y/2.0)
                    wins = wins + 1 #Let's consider it a win


                else:
                    losses = losses + 1

            else: #Then I believe that y is more

                if( y > x):
                    cash = cash + y #Then I win y dollars
                    wins = wins + 1
                    earnings.append(y)

                elif( y == x):
                    cash = cash + y/2.0
                    earnings.append(y/2.0)
                    wins = wins + 1 #Let's consider it a win


                else:
                    losses = losses + 1

            trackOfCash.append(cash)
            trackOfProbability.append(wins/float(attempt+1))

        ##############
        #and also be a random player

            if(random() > 0.5):
                if( y < x ): #If my guess was correct
                    cashRandom = cashRandom + y #Then I win y dollars
                    winsRandom = winsRandom + 1
                elif( y == x):
                    cashRandom = cashRandom + y/2.0
                    winsRandom = winsRandom + 1
                else:
                    lossesRandom = lossesRandom + 1

            else:

                if( y > x):
                    cashRandom = cashRandom + y #Then I win y dollars
                    winsRandom = winsRandom + 1
                elif( y == x):
                    cashRandom = cashRandom + y/2.0
                    winsRandom = winsRandom + 1
                else:
                    lossesRandom = lossesRandom + 1

            trackOfCashRandom.append(cashRandom)

        cashEachEpisode.append(trackOfCash)
        probabilityOfWinningEachEpisode.append(trackOfProbability)
        cashEachEpisodeRandom.append(trackOfCashRandom)

    print '\n\nStatistical Analysis\n---------------------------------\n'

    print 'Number of Episodes: ', numOfEpisodes
    print 'Number of attempts in each episode: ', numOfAttempts

    print '\n\nRational Player (last attempt shown)\n-------------------------'
    print 'Max cash: ', np.max(cashEachEpisode[-1])
    print 'Min cash: ', np.min(cashEachEpisode[-1])
    print 'Wins: ', wins
    print 'Losses: ', losses
    print 'Probability of winning: ', wins/float(numOfAttempts)

    #print 'Earnings: ', earnings

    try:
        print 'Mean Value of earnings: ', np.mean(earnings)
        print 'Standard deviation of earnings: ', np.std(earnings)
    except:
        pass #Earnings vector is empty


    print '\n\nRandom Player (last attempt shown)\n-------------------------'
    print 'Wins: ', winsRandom
    print 'Losses: ', lossesRandom
    print 'Probability of winning: ', winsRandom/float(numOfAttempts)

    print '\n\n'
    
    pylab.figure()
    pylab.errorbar(pylab.arange(len(cashEachEpisode[0])), np.mean(cashEachEpisode, 0), np.std(cashEachEpisode, 0))
    pylab.errorbar(pylab.arange(len(cashEachEpisodeRandom[0])), np.mean(cashEachEpisodeRandom, 0), np.std(cashEachEpisodeRandom, 0))
    pylab.title('Cash for ' + str(numOfAttempts) + ' attempts and ticket price $' + str(ticketPrice) )
    #pylab.legend([p1, p2], ["Rational", "Random"])

    #print probabilityOfWinningEachEpisode
    pylab.figure()
    pylab.errorbar(pylab.arange(len(probabilityOfWinningEachEpisode[0])), np.mean(probabilityOfWinningEachEpisode, 0), np.std(probabilityOfWinningEachEpisode, 0))
    #pylab.errorbar(pylab.arange(len(cashEachEpisodeRandom[0])), np.mean(cashEachEpisodeRandom, 0), np.std(cashEachEpisodeRandom, 0))
    pylab.title('Probability of winning in an episode')
    #pylab.legend([p1, p2], ["Rational", "Random"])
    pylab.show()


    pylab.show()


if( __name__ == "__main__"):
    main()

